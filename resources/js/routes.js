import HomePage from './components/Pages/Home';
import Login from './components/Pages/Login';
import Signup from './components/Pages/Signup';
import Account from './components/Pages/Account';
import Dashboard from './components/Pages/Dashboard';
import PaymentGateway from './components/Pages/PaymentGateway';
import CourseView from './components/Pages/CourseView';
import Videos from './components/Pages/videos';
import Logout from './components/Pages/Logout';
import AdminHome from './components/Pages/AdminHome';
import AdminAllCourses from './components/Pages/AdminAllCourses';
import AdminAllUsers from './components/Pages/AdminAllUsers';
import AdminCourseView from './components/Pages/AdminCourseView';
import AdminCourseAdd from './components/Pages/AdminCourseAdd';
import ViewUser from './components/Pages/ViewUser';
import UserCourseView from './components/Pages/UserCourseView';
import AdminCoupons from './components/Pages/AdminCoupons';

const routes = [
    {
        path:"/",
        name:"Home",
        component:HomePage,
        meta: { 
            Any: true,
        }
    },
    {
        path:"/login",
        name:"Login",
        component:Login,
    },
    {
        path:"/join",
        name:"Join",
        component:Signup,
        meta: { 
            Guest: true,
        }
    },
    {
        path:"/dashboard",
        name:"Dashboard",
        component:Dashboard,
        meta: { 
            Auth: true,
        }
    },
    {
        path:"/account",
        name:"Account",
        component:Account,
        meta: { 
            Auth:true,
        }
    },
    {
        path:"/checkout/:id",
        name:"Order Summary",
        component:PaymentGateway,
        meta: { 
            Auth:true,
        }
    },
    {
        path:"/course/:id",
        name:"Access Course",
        component:CourseView,
        meta: { 
            Auth:true,
        }
    },
    {
        path:"/video",
        name:"Videos",
        component:Videos,
        meta: { 
            Auth:true,
        }
    },
    {
        path:"/logout",
        name:"LogOut",
        component:Logout,
        meta: { 
            Auth: true,
        }
    },
    {
        path:"/admin",
        name:"Admin",
        component: AdminHome,
        meta: { 
            Auth: true,
        },
        children: [
            {
                name:"AdminCourses",
                path: 'courses',
                component: AdminAllCourses,
                children: [
                ]
            },
            {
                name:"AdminCourseAdd",
                path: 'create/course',
                component: AdminCourseAdd
            },
            {
                name:"AdminCourseView",
                path: 'course/:course',
                component: AdminCourseView
            },
            {
                name:"AdminUsers",
                path: 'users',
                component: AdminAllUsers
            },
            {
                name:"ViewUser",
                path: 'user/:id',
                component: ViewUser,
                children: [
                    {
                        name:"UserCourseView",
                        path: ':course',
                        component: UserCourseView
                    },      
                ]
            },
            {
                name:"AdminCoupons",
                path: 'coupons',
                component: AdminCoupons
            },
        ],
    },
];

export default routes;