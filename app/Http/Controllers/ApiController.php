<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Auth;
use App\User;
class ApiController extends Controller
{
    /*
    /*  Login via API . 
    */
    public function Login(Request $request)
    {
        $http = new \GuzzleHttp\Client;

        try {
            $response = $http->post(route('passport.token'),[
                'form_params' => [
                    'grant_type'    => "password",
                    'client_id'     => config("services.passport.client.id"),
                    'client_secret' => config("services.passport.client.secret"),
                    'username'      => $request->username,
                    'password'      => $request->password,
                ]
            ]);
            return $response->getBody();
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            if($e->getCode() == 400){
                return response()->json("Invalid Request. Please Enter a username and/or password $request->password",$e->getCode());
            }
            else if($e->getCode() == 401){
                return response()->json("Your username and/or password is incorrect",$e->getCode());
            }
            else{
                return response()->json("Something Went wrong",$e->getCode());
            }
        }

    }
    /*
    /*  Register via API . 
    */
    public function Register(Request $request)
    {
        // return $request;
        $request->validate([
            "name"=>"required|string|max:255",
            "email"=>"required|string|email|unique:users",
            "password"=>"required|string|min:6",
            "phone"=>"required|string|size:10",
        ],[
            'name.required' => 'Please Enter your Name',
            'email.required' => 'We need to know your e-mail address!',
            'email.unique' => 'This Email address is already registered. Please login',
            'password.required' => 'A strong password is required',
            'password.min' => 'Password Requires atleast 6 chars.',
            'phone.required' => 'Phone no is not valid. Please check ',
            'phone.size' => 'Phone no is not valid. Please check ',
        ]);
        
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->phone = $request->phone;
        $user->save();
        // return $request->password;
        return response()->json("Account Creation successful",200);
    }
    /*
    /*  Logout via API . 
    */
    public function Logout(Request $request)
    {
        return auth()->user()->tokens;
        auth()->user()->tokens->each(function($token,$key){
          $token->delete();
        });
      return response()->json("Logout Success",200);
    }
    /*
    /*  Request via API . 
    */
    public function Request(Request $request)
    {
        
        // return $request->headers->all();
        $http = new \GuzzleHttp\Client(['headers' => ["Authorization" =>"Basic $request->token"]]);
        $response = $http->{$request->method()}($request->url,[
            'form_params'=>$request->except('token')
        ]);
        return $response;
    }
    /*
    /*  Verify Email 
    */
    public function EmailVerify(Request $request)
    {
        $email = $request->email;
        $user  = User::where('email',$email)->first();
        if($user){

        }
        else{
            
        }
    }
    /*
    /*  Verify Email 
    */
    public function Users(Request $request)
    {
        return User::all();
    }
    /*
    /*  Verify Email 
    */
    public function User($email)
    {
        $user = User::where('email',$email)->first();
        $user->courses = $user->Courses;
        foreach ($user->courses as $key) {
            $key->course = $key->Course;
        }
        return $user;
    }
    /*
    /*  Verify Email 
    */
    public function UpdateUser(Request $request, $id)
    {
        $user = User::findorfail($id);      
        $data = $request->user;
        $user->name = $data['name'];
        if($data['password']){
            $user->password = Hash::make($data['password']);
        };
        $user->phone = $data['phone'];
        $user->save();
        return response()->json($user,200);
    }
}
